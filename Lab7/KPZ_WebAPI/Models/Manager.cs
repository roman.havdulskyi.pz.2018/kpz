﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Models
{
    public class Manager 
    {
        public int Id { get; set; }

        public String Name { set; get; }

        public String Surname { set; get; }

        public String Address { set; get; }

        public int Status { set; get; }

      
    }
}
