﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Models
{
    public class User
    {
        public int Id { get; set; }

        public String Name { set; get; }
        public String Surname { set; get; }
        public double Balance { set; get; }
        public string Address { set; get; }
        public string TariffName { set; get; }
        public int Status { set; get; }

    }
}
