﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Models
{
    public class Tariff 
    {
        public int Id { get; set; }

        public String Name { set; get; }

        public int Speed { set; get; }

        public double Price { set; get; }

        public double DiscountPrice { set; get; }

        public int ActivateDiscount { set; get; }
    }
}
