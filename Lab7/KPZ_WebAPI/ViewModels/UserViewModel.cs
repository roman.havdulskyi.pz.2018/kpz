﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public String Username { set; get; }
        public double Balance { set; get; }
        public string Address { set; get; }
        public string TariffName { set; get; }
        public int Status { set; get; }
    }
}
