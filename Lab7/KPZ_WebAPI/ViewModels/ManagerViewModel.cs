﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.ViewModels
{
    public class ManagerViewModel
    {
        public int Id { get; set; }

        public String Username { set; get; }

        public String Address { set; get; }

        public int Status { set; get; }

    }
}
