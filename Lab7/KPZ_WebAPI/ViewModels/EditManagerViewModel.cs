﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.ViewModels
{
    public class EditManagerViewModel
    {
        [Required(ErrorMessage = "Please, type a manager id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please, type a manager name")]
        public String Name { set; get; }

        [Required(ErrorMessage = "Please, type a manager surname")]
        public String Surname { set; get; }

        [Required(ErrorMessage = "Please, type a manager address")]
        public String Address { set; get; }

        public int Status { set; get; }

    }
}
