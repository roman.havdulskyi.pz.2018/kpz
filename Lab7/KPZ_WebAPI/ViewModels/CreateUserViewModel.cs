﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.ViewModels
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "Please, type a user name")]
        public String Name { set; get; }

        [Required(ErrorMessage = "Please, type a user surname")]
        public String Surname { set; get; }
        public double Balance { set; get; }

        [Required(ErrorMessage = "Please, type a user address")]
        public string Address { set; get; }
        public string TariffName { set; get; }
        public int Status { set; get; }
    }
}
