﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.ViewModels
{
    public class EditTariffViewModel
    {
        [Required(ErrorMessage = "Please, type a tariff id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please, type a tariff name")]
        public String Name { set; get; }

        [Required]
        [Range(5, 1000, ErrorMessage = "Please, type a tariff speed")]
        public int Speed { set; get; }

        [Required]
        [Range(5, 1000, ErrorMessage = "Please, type a tariff price")]
        public double Price { set; get; }

        public double DiscountPrice { set; get; }

        public int ActivateDiscount { set; get; }
    }
}
