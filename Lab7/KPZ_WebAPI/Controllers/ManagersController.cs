﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KPZ_WebAPI.Data;
using KPZ_WebAPI.Models;
using AutoMapper;
using KPZ_WebAPI.ViewModels;

namespace KPZ_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManagersController : ControllerBase
    {
        private readonly IRepository<Manager> _repository;
        private readonly IMapper _mapper;


        public ManagersController(IRepository<Manager> repository)
        {
            _repository = repository;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Manager, ManagerViewModel>()
                     .ForMember("Username", opt => opt.MapFrom(c => c.Name + " " + c.Surname));
                cfg.CreateMap<Manager, EditManagerViewModel>();
                cfg.CreateMap<EditManagerViewModel, Manager>();
                cfg.CreateMap<CreateManagerViewModel, Manager>();

            });

            _mapper = new Mapper(config);
        }

        /// <summary>
        /// Get all Managers.
        /// </summary>
        // GET: api/Managers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerViewModel>>> GetManagers()
        {
            var users = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Manager>, List<ManagerViewModel>>(users);
        }

        /// <summary>
        /// Get a specific Manager.
        /// </summary>
        // GET: api/Managers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ManagerViewModel>> GetManager(int id)
        {
            var manager = await _repository.GetAsync(id);

            if (manager == null)
            {
                return NotFound();
            }

            return _mapper.Map<Manager, ManagerViewModel>(manager); ;
        }

        /// <summary>
        /// Update a specific Manager.
        /// </summary>
        // PUT: api/Managers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutManager(int id, [FromForm] EditManagerViewModel editManagerViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var manager = _mapper.Map<EditManagerViewModel, Manager>(editManagerViewModel);
            if (id != manager.Id)
            {
                return BadRequest();
            }


            try
            {
                await _repository.UpdateAsync(manager);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ManagerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new Manager.
        /// </summary>
        // POST: api/Managers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ManagerViewModel>> PostManager([FromForm] CreateManagerViewModel createManagerViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var manager = _mapper.Map<CreateManagerViewModel, Manager>(createManagerViewModel);
            await _repository.CreateAsync(manager);

            return CreatedAtAction("GetManager", new { id = manager.Id }, _mapper.Map<Manager, ManagerViewModel>(manager));
        }

        /// <summary>
        /// Delete a specific Manager.
        /// </summary>
        // DELETE: api/Managers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteManager(int id)
        {
            var manager = await _repository.GetAsync(id);
            if (manager == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(manager);

            return NoContent();
        }

        private bool ManagerExists(int id)
        {
            return _repository.Exist(id);
        }
    }
}
