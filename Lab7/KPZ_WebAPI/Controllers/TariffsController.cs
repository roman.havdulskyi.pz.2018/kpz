﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KPZ_WebAPI.Data;
using KPZ_WebAPI.Models;
using AutoMapper;
using KPZ_WebAPI.ViewModels;

namespace KPZ_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TariffsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private IRepository<Tariff> _repository;

        public TariffsController(IRepository<Tariff> repository)
        {
            _repository = repository;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tariff, TariffViewModel>();
                cfg.CreateMap<Tariff, EditTariffViewModel>();
                cfg.CreateMap<EditTariffViewModel, Tariff>();
                cfg.CreateMap<CreateTariffViewModel, Tariff>();

            });

            _mapper = new Mapper(config);
        }

        /// <summary>
        /// Get all Tariffs.
        /// </summary>
        // GET: api/Tariffs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TariffViewModel>>> GetTariff()
        {
            var tariffs = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Tariff>, List<TariffViewModel>>(tariffs);
        }

        /// <summary>
        /// Get a specific Tariff.
        /// </summary>
        // GET: api/Tariffs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TariffViewModel>> GetTariff(int id)
        {
            var tariff = await _repository.GetAsync(id);

            if (tariff == null)
            {
                return NotFound();
            }

            return _mapper.Map<Tariff, TariffViewModel>(tariff);
        }

        /// <summary>
        /// Update a specific Tariff.
        /// </summary>
        // PUT: api/Tariffs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTariff(int id, [FromForm] EditTariffViewModel editTariffViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var tariff = _mapper.Map<EditTariffViewModel, Tariff>(editTariffViewModel);

            if (id != tariff.Id)
            {
                return BadRequest();
            }


            try
            {
                await _repository.UpdateAsync(tariff);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_repository.Exist(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new Tariff.
        /// </summary>
        // POST: api/Tariffs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TariffViewModel>> PostTariff([FromForm] CreateTariffViewModel createTariffViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var tariff = _mapper.Map<CreateTariffViewModel, Tariff>(createTariffViewModel);
            await _repository.CreateAsync(tariff);
       

            return CreatedAtAction("GetTariff", new { id = tariff.Id }, _mapper.Map<Tariff, TariffViewModel>(tariff));
        }

        /// <summary>
        /// Delete a specific Tariff.
        /// </summary>
        // DELETE: api/Tariffs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTariff(int id)
        {
            var tariff = await _repository.GetAsync(id);
            if (tariff == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(tariff);

            return NoContent();
        }

    }
}
