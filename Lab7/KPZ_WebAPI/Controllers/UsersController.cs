﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KPZ_WebAPI.Data;
using KPZ_WebAPI.Models;
using AutoMapper;
using KPZ_WebAPI.ViewModels;

namespace KPZ_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repository;
        private readonly IMapper _mapper;


        public UsersController(IRepository<User> repository)
        {
            _repository = repository;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserViewModel>()
                     .ForMember("Username", opt => opt.MapFrom(c => c.Name + " " + c.Surname));
                cfg.CreateMap<User, EditUserViewModel>();
                cfg.CreateMap<EditUserViewModel, User>();
                cfg.CreateMap<CreateUserViewModel, User>();

            });
       
            _mapper = new Mapper(config);
        }

        /// <summary>
        /// Get all Users.
        /// </summary>
        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> GetUsers()
        {
            var users = await _repository.GetAll();
            return _mapper.Map<IEnumerable<User>, List<UserViewModel>>(users);
        }

        /// <summary>
        /// Get a specific User.
        /// </summary>
        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserViewModel>> GetUser(int id)
        {
            var user = await _repository.GetAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<User, UserViewModel>(user);
        }

        /// <summary>
        /// Update a specific User.
        /// </summary>
        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, [FromForm] EditUserViewModel editUserViewModel)
        {
            var user = _mapper.Map<EditUserViewModel, User>(editUserViewModel);

            if (id != user.Id)
            {
                return BadRequest();
            }

            try
            {
                await _repository.UpdateAsync(user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new User.
        /// </summary>
        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UserViewModel>> PostUser([FromForm] CreateUserViewModel createUserViewModel)
        {
            var user = _mapper.Map<CreateUserViewModel, User>(createUserViewModel);
            await _repository.CreateAsync(user);

            return CreatedAtAction("GetUser", new { id = user.Id }, _mapper.Map<User, UserViewModel>(user));
        }

        /// <summary>
        /// Delete a specific User.
        /// </summary>
        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _repository.GetAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(user);

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return _repository.Exist(id);
        }
    }
}
