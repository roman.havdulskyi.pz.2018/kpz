﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KPZ_WebAPI.Models;

namespace KPZ_WebAPI.Data
{
    public class KPZ_WebAPIContext : DbContext
    {
        public KPZ_WebAPIContext (DbContextOptions<KPZ_WebAPIContext> options)
            : base(options)
        {
        }


        public DbSet<Models.Tariff> Tariffs { get; set; }
        public DbSet<Models.User> Users { get; set; }
        public DbSet<Models.Manager> Managers { get; set; }
    }
}
