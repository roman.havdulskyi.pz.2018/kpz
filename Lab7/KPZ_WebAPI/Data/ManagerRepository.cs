﻿using KPZ_WebAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Data
{
    public class ManagerRepository : IRepository<Manager>
    {

        private readonly KPZ_WebAPIContext _context;

        public ManagerRepository(KPZ_WebAPIContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(Manager item)
        {
            _context.Managers.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Manager manager)
        {
            _context.Managers.Remove(manager);
            await _context.SaveChangesAsync();
        }

        public async Task<Manager> GetAsync(int id)
        {
            return await _context.Managers.FindAsync(id);
        }

        public async Task<List<Manager>> GetAll()
        {
            return await _context.Managers.ToListAsync();
        }


        public async Task UpdateAsync(Manager item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool Exist(int id)
        {
            return _context.Managers.Any(e => e.Id == id);
        }

        
    }
}
