﻿using KPZ_WebAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Data
{
    public class TariffRepository : IRepository<Tariff>
    {
        private readonly KPZ_WebAPIContext _context;

        public TariffRepository(KPZ_WebAPIContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Tariff item)
        {
            _context.Tariffs.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Tariff tariff)
        {
            _context.Tariffs.Remove(tariff);
            await _context.SaveChangesAsync();
        }

        public async Task<Tariff> GetAsync(int id)
        {
            return await _context.Tariffs.FindAsync(id);
        }

        public async Task<List<Tariff>> GetAll()
        {
            return await _context.Tariffs.ToListAsync();
        }

      
        public async Task UpdateAsync(Tariff item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool Exist(int id)
        {
            return _context.Tariffs.Any(e => e.Id == id);
        }
    }
}
