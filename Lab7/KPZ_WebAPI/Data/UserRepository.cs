﻿using KPZ_WebAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_WebAPI.Data
{
    public class UserRepository : IRepository<User>
    {

        private readonly KPZ_WebAPIContext _context;

        public UserRepository(KPZ_WebAPIContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(User item)
        {
            _context.Users.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(User user)
        {
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetAsync(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<List<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }


        public async Task UpdateAsync(User item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool Exist(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        
    }
}
