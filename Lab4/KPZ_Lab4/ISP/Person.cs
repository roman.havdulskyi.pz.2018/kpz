﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab4.ISP
{
    abstract class Person
    {

        public String ID { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }
        public String FatherName { set; get; }
        public Address Address { set; get; }
        public Contract Contract { set; get; }

        public abstract Permissions GetPermissions();

    }
}
