﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab4.ISP
{
    class Address
    {
        public String Country { set; get; }
        public String Region { set; get; }
        public String City { set; get; }
        public String Street { set; get; }
        public String StreetId { set; get; }
        public String BuildingNumber { set; get; }
        public String FlatNumber { set; get; }

    }
}
