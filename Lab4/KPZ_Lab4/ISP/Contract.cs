﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab4.ISP
{
    class Contract
    {
        public String ContractID { set; get; }
        public DateTimeOffset ContractSigned { set; get; }
        public DateTimeOffset ContractTerm { set; get; }
        public bool IsValid { set; get; }

    }
}
