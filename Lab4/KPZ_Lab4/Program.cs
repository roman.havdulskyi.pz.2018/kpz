﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 243;
            Object Obj = num;

            Console.WriteLine(Obj);


            Console.WriteLine($"^ with enums =  {ClassID.FIRST ^ ClassID.SECOND}");
            Console.WriteLine($"& with enums =  {ClassID.FIRST & ClassID.SECOND}");
            Console.WriteLine($"| with enums = {ClassID.FIRST | ClassID.SECOND}");


            Console.WriteLine("A");
            A a = new A();
            a.TestAccess();
            Console.WriteLine("B");
            B b = new B();
            b.TestAccess();
            Console.WriteLine("C");
            C c = new C();
            c.TestAccess();


            var str = "string value";
            TestWithoutRef(str);
            Console.WriteLine(str);

            String str1;
            str1 = "string value";
            TestWithRef(ref str1);
            Console.WriteLine(str1);

            String str2;
            TestWithOut(out str2);
            Console.WriteLine(str2);

            var charString = new CharString("test string");

            string testString = charString;
            Console.WriteLine(testString);  

            CharString charString1 = (CharString)testString;
            Console.WriteLine(charString1);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 10_000_000; i++)
            {
                using (var value = new TestStructure())
                {

                }
            }

            stopwatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

             stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 10_000_000; i++)
            {
                using(var value = new TestClass())
                {

                }
            }

            stopwatch.Stop();
            // Get the elapsed time as a TimeSpan value.
             ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value.
             elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            for (int i = 0; i < 10_000_000; i++)
            {
                using (var value = new TestSealedClass())
                {

                }
            }

            stopwatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value.
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
               ts.Hours, ts.Minutes, ts.Seconds,
               ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            Console.ReadLine();

        }

        public struct TestStructure : IDisposable
        {
            public String Value;

            public void Dispose()
            {
                Value = null;
            }
        }

        public class TestClass : IDisposable
        {
            public String Value;

            public void Dispose()
            {
                Value = null;
            }
        }

        public sealed class TestSealedClass : IDisposable
        {
            public String Value;

            public void Dispose()
            {
                Value = null;
            }
        }

        public class CharString
        {
            public List<Char> Chars { set; get; }

            public CharString(string str)
            {
                Chars = new List<char>();
                foreach(char item in str.ToCharArray())
                {
                    Chars.Add(item);
                }
            }

            public static implicit operator string(CharString charString) => charString.Chars.Aggregate("", (acc, it) => acc + it);
         
            public static explicit operator CharString(string str) => new CharString(str);

            public override string ToString() => $"{Chars.Aggregate("", (acc, it) => acc + it)}";
        }

        private static void TestWithoutRef(String str)
        {
            str = "no ref";
        }

        private static void TestWithRef(ref String str)
        {
            str = "ref";
        }

        private static void TestWithOut(out String str)
        {
            str = "out";
        }

        enum ClassID
        {
            FIRST, SECOND, THIRD, FOURTH, FIVETH
        }

        interface Printable
        {
            void PrintClass();
        }
        
        public abstract class AccessCheckable
        {
           public abstract void TestAccess();
        }

        public class A : AccessCheckable, Printable
        {
            public String PublicString { set; get; }

            protected String ProtectedString { set; get; }

            private String PrivateString { set; get; }

            String UndefinedModifierString { set; get; }

            NestedClass nestedClassObject { set; get; }

            static String StaticString { set; get; }


            static A() {
                StaticString = "A";
                Console.WriteLine("Init static fields");
            }

            public A()
            {
                PublicString = "-";
                ProtectedString = "-";
                PrivateString = "-";
                UndefinedModifierString = "-";
                Console.WriteLine("Dynamic constuctor A");
            }


            public A(String a)
            {
                this.PublicString = a;
                Console.WriteLine("Dynamic constuctor A");
            }

            public void PublicMethod()
            {
                Console.WriteLine("Public method");
            }

            protected void ProtectedMethod()
            {
                Console.WriteLine("Protected method");
            }

            private void PrivateMethod()
            {
                Console.WriteLine("Private method");
            }

            void UndefinedModifierMethod()
            {
                Console.WriteLine("UndefinedModifier method");
            }

            public override void TestAccess()
            {
                Console.WriteLine($"Begin test access");
                PublicString = "A";
                ProtectedString = "A";
                PrivateString = "A";
                UndefinedModifierString = "A";
                PublicMethod();
                ProtectedMethod();
                PrivateMethod();
                UndefinedModifierMethod();

                nestedClassObject =  new NestedClass();

                PrintClass();
                Console.WriteLine("End test access");
            }

            public override string ToString()
            {
                return $"{base.ToString()}: PublicString =  {PublicString} \n" +
                    $"ProtectedString =  {ProtectedString} \n" +
                    $"PrivateString = {PrivateString} \n" +
                    $"UndefinedModifierString =  {UndefinedModifierString} \n" +
                    $"nestedClassObject = {nestedClassObject}";
            }

            public void PrintClass()
            {
                Console.WriteLine(this.ToString());
            }

            internal class NestedClass
            {

                public NestedClass()
                {
                    Console.WriteLine("Hello, I`m nested class. You have just an access to me )");
                }

            }

        }

        public class B : A
        {

            public B(String a) : base(a)
            {
                Console.WriteLine("Dynamic constuctor B");
            }

            public B(String a, String b) : this(a)
            {
                this.ProtectedString = b;
                Console.WriteLine("Dynamic constuctor B");
            }

            public B()
            {
                Console.WriteLine("Dynamic constuctor B");
            }

            public override string ToString()
            {
                return $"{base.ToString()}:\n" +
                    $" PublicString =  {PublicString}\n" +
                    $" ProtectedString = {ProtectedString}";
            }

            public override void TestAccess()
            {
                Console.WriteLine($"Begin test access");

                PublicString = "B";
                ProtectedString = "B";


                PublicMethod();
                ProtectedMethod();

                PrintClass();

                Console.WriteLine("End test access");
            }


        }

        public class C : AccessCheckable
        {
            public override void TestAccess()
            {
                Console.WriteLine("Begin test access");

                A a = new A();
                B b = new B();
                a.PublicString = "C";
                a.PublicMethod();
                a.PrintClass();

                b.PublicString = "B";
                b.PrintClass();

                Console.WriteLine("End test access");
            }
        }


    }
}
