﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;

namespace Lab3_KPZ
{
    class Program
    {
        static void Main(string[] args)
        {
            DataProducer dataProducer = new DataProducer();

            //1
            Console.WriteLine("//1 All drinks with repetitions:");
            dataProducer.Сategories
                .ToList()
                .ForEach(it =>
                {
                    var result = from d in dataProducer.Drinks
                        where d.CategoryId == it.Id
                        orderby d.Name
                        select d;
                    result.ToList().ForEach(Console.WriteLine);
                });

            //2
            Console.WriteLine("\n");
            Console.WriteLine("//2 All drinks formatted as [Name : CategoryID]:");
            dataProducer.Сategories
                .ToList()
                .ForEach(it =>
                {
                    var result = from d in dataProducer.Drinks
                        where d.CategoryId == it.Id
                        orderby d.Name
                        group d by new {d.Name}
                        into grouped
                        select grouped.FirstOrDefault();
                    var dictionary = result.ToList().ToDictionary(t => t.Name, t => t.CategoryId);
                    foreach (var keyValuePair in dictionary)
                        Console.WriteLine(keyValuePair);
                });

            //3
            Console.WriteLine("\n");
            Console.WriteLine("//3 Drinks grouped by Categories:");

            var drinksGrouped0 = from d in dataProducer.Drinks
                orderby d.CategoryId
                group d by d.CategoryId
                into grouped
                select new
                {
                    Key = grouped.Key,
                    List = grouped.OrderBy(it=>it.Name).ToList(),
                };
            foreach (var item in drinksGrouped0)
            {
                Console.WriteLine($"{item.Key} : ");
                item.List.ForEach(Console.WriteLine);
            }

            //4
            Console.WriteLine("\n");
            Console.WriteLine("//4 Drinks grouped by Name:");

            var drinksGrouped = from d in dataProducer.Drinks
                orderby d.Name
                group d by d.Name
                into grouped
                select new
                {
                    Key = grouped.Key,
                    Count = grouped.Count(),
                };
            drinksGrouped.ToList().ForEach(Console.WriteLine);

            //5
            Console.WriteLine("\n");
            Console.WriteLine("//5 Drinks from category where id 4:");

            var drinksGrouped2 = from d in dataProducer.Drinks
                orderby d.Name
                group d by d.CategoryId
                into grouped
                select new
                {
                    Key = grouped.Key,
                    List = grouped,
                };
            var lookedUpDrinks = drinksGrouped2.ToLookup(it => it.Key);
            foreach (var item in lookedUpDrinks[4])
            {
                Console.WriteLine($"{item.Key}");
                item.List.ToList().ForEach(Console.WriteLine);
            }

            //6
            Console.WriteLine("\n");
            Console.WriteLine("//6 All drinks name without repetitions:");
            dataProducer.Сategories
                .ToList()
                .ForEach(it =>
                {
                    var result = from d in dataProducer.Drinks
                        where d.CategoryId == it.Id
                        orderby d.Name
                        group d by new {d.Name}
                        into grouped
                        select grouped.FirstOrDefault();
                    result.ToList().ForEach(Console.WriteLine);
                });

            //7
            Console.WriteLine("\n");
            Console.WriteLine("//7 Beverages drinks:");
            dataProducer.Сategories
                .Where(it => it.Name == "Beverages")
                .ToList()
                .ForEach(it =>
                {
                    var result = from d in dataProducer.Drinks
                        where d.CategoryId == it.Id
                        orderby d.Name
                        select d.Name;
                    result.ToList().ForEach(Console.WriteLine);
                });

            //8
            Console.WriteLine("\n");
            Console.WriteLine("//8 All drinks names: ");
            dataProducer.Drinks.Select(it => it.Name).ToList().ForEach(Console.WriteLine);

            //9
            Console.WriteLine("\n");
            Console.WriteLine("//9 Drinks started with P char");
            dataProducer.Drinks.Where(it => it.Name.StartsWith("P")).ToList().ForEach(Console.WriteLine);

            //10
            Console.WriteLine("\n");
            Console.WriteLine("//10 Sorted items");
            var items = dataProducer.Drinks;
            dataProducer.Drinks.OrderBy(t=> t.Name).ToList().ForEach(Console.WriteLine);

            //11
            Console.WriteLine("\n");
            Console.WriteLine("//11 Sorted items as array");
            var items2 = dataProducer.Drinks;
            items2.Sort((it, it1) => String.Compare(it.Name, it1.Name, StringComparison.Ordinal));
            var array = items2.ToArray();
            foreach (var drink in array)
            {
                Console.WriteLine(drink);    
            }

            const int aAsciiCode = 65;


            var str =  String.Join( ",", Enumerable.Range(aAsciiCode, 6).Select(it => (char)it).ToList());
            Console.WriteLine(str);
            Console.ReadLine();
        }


        public class Drinks
        {
            public Drinks()
            {
            }

            public string Name { get; set; }
            public int CategoryId { get; set; }

            public override string ToString()
            {
                return $"Name = {Name} CategoryID = {CategoryId}";
            }
        }


        public class Category
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }


        public class DataProducer
        {
            public List<Category> Сategories =>
                new List<Category>
                {
                    new Category {Name = "Water", Id = 001},
                    new Category {Name = "Juice", Id = 002},
                    new Category {Name = "Beverages", Id = 003},
                    new Category {Name = "Hot drinks", Id = 004},
                    new Category {Name = "Energetic", Id = 005},
                    new Category {Name = "Milk", Id = 006},
                    new Category {Name = "Sweet", Id = 007},
                };

            // Specify the second data source.
            public List<Drinks> Drinks =>
                new List<Drinks>
                {
                    new Drinks {Name = "Water", CategoryId = 001},
                    new Drinks {Name = "Espresso", CategoryId = 004},
                    new Drinks {Name = "Espresso", CategoryId = 004},
                    new Drinks {Name = "Water with lemon", CategoryId = 001},
                    new Drinks {Name = "Berry juice", CategoryId = 002},
                    new Drinks {Name = "Coca cola", CategoryId = 007},
                    new Drinks {Name = "Coca cola", CategoryId = 007},
                    new Drinks {Name = "Vegetables juice", CategoryId = 002},
                    new Drinks {Name = "Beer", CategoryId = 003},
                    new Drinks {Name = "Vodka", CategoryId = 003},
                    new Drinks {Name = "Black tea", CategoryId = 004},
                    new Drinks {Name = "Green tea", CategoryId = 004},
                    new Drinks {Name = "America", CategoryId = 004},
                    new Drinks {Name = "Glace", CategoryId = 004},
                    new Drinks {Name = "Mocha", CategoryId = 004},
                    new Drinks {Name = "Irish", CategoryId = 004},
                    new Drinks {Name = "Frappe", CategoryId = 004},
                    new Drinks {Name = "Coffee milk shake", CategoryId = 004},
                    new Drinks {Name = "Cappucino", CategoryId = 004},
                    new Drinks {Name = "Hot chocolate", CategoryId = 004},
                    new Drinks {Name = "Redbull", CategoryId = 005},
                    new Drinks {Name = "Whiskey", CategoryId = 003},
                    new Drinks {Name = "Rome", CategoryId = 003},
                    new Drinks {Name = "Pitbull", CategoryId = 005},
                    new Drinks {Name = "Monster", CategoryId = 005},
                    new Drinks {Name = "Non stop", CategoryId = 005},
                    new Drinks {Name = "Milk skim", CategoryId = 006},
                    new Drinks {Name = "Milk 1%", CategoryId = 006},
                    new Drinks {Name = "Milk 2%", CategoryId = 006},
                    new Drinks {Name = "Milk whole", CategoryId = 006},
                    new Drinks {Name = "Iced coffee", CategoryId = 004},
                    new Drinks {Name = "Mineral water", CategoryId = 001},
                    new Drinks {Name = "Mineral water", CategoryId = 001},
                    new Drinks {Name = "Fanta", CategoryId = 007},
                    new Drinks {Name = "Sprite", CategoryId = 007},
                    new Drinks {Name = "Sokovinka", CategoryId = 007},
                    new Drinks {Name = "Latte", CategoryId = 004},
                    new Drinks {Name = "Latte", CategoryId = 004},
                    new Drinks {Name = "Flat white", CategoryId = 004}
                };
        }
    }
}