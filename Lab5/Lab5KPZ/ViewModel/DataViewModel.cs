﻿using Lab5KPZ.Commands;
using Lab5KPZ.Model;
using Lab5KPZ.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab5KPZ.ViewModel
{
    class DataViewModel : ViewModelBase
    {

        public DataViewModel()
        {
            SetControlVisible = new Command(ChangeControlVisible);
        }

        private string _VisibleControl = View.VisibleControl.Users.ToString();

        public string VisibleControl
        {
            get
            {
                return _VisibleControl;
            }

            set
            {
                _VisibleControl = value;
                OnPropertyChanged("visibleControl");
            }
        }

        private TariffViewModel _SelectedTariff = null;

        public TariffViewModel SelectedTariff
        {
            get
            {
                return _SelectedTariff;
            }

            set
            {
                if (SelectedUser != null && value != null)
                    SelectedUser.TariffName = value.Name;

                _SelectedTariff = value;
                OnPropertyChanged("SelectedTariff");
            }
        }

        private UserViewModel _SelectedUser = null;

        public UserViewModel SelectedUser
        {
            get
            {
                return _SelectedUser;
            }

            set
            {
                SelectedTariff = null;

                _SelectedUser = value;
                OnPropertyChanged("SelectedUser");
            }
        }

        private ManagerViewModel _SelectedManager = null;

        public ManagerViewModel SelectedManager
        {
            get
            {
                return _SelectedManager;
            }

            set
            {
                _SelectedManager = value;
                OnPropertyChanged("SelectedManager");
            }
        }

        public ICommand SetControlVisible { set; get; }

        public void ChangeControlVisible(object arg)
        {
            VisibleControl = arg.ToString();
            PlaySoundClick();
        }

        public void PlaySoundClick()
        {
            var notificationSound = new SoundPlayer(Properties.Resources.click);
            notificationSound.Play();
        }

        private ObservableCollection<UserViewModel> _Users;

        public ObservableCollection<UserViewModel> Users
        {
            get
            {
                return _Users;
            }

            set
            {
                this._Users = value;
                OnPropertyChanged("Users");
            }
        }
        
        private ObservableCollection<TariffViewModel> _Tariffs;

        public ObservableCollection<TariffViewModel> Tariffs
        {
            get
            {
                return _Tariffs;
            }

            set
            {
                this._Tariffs = value;
                OnPropertyChanged("Tariffs");
            }
        }

        private ObservableCollection<ManagerViewModel> _Managers;

        public ObservableCollection<ManagerViewModel> Managers
        {
            get
            {
                return _Managers;
            }

            set
            {
                this._Managers = value;
                OnPropertyChanged("Managers");
            }
        }
    }
}
