﻿using Lab5KPZ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.ViewModel
{
    class UserViewModel : ViewModelBase
    {

        private String _Name;
        public String Name
        {
            get
            {
                return _Name;
            }

            set
            {
                this._Name = value;
                OnPropertyChanged("Name");
            }
        }

        private String _Surname;

        public String Surname
        {
            get
            {
                return _Surname;
            }

            set
            {
                this._Surname = value;
                OnPropertyChanged("Surname");
            }
        }

        private double _Balance;
        public double Balance
        {
            get
            {
                return _Balance;
            }

            set
            {
                this._Balance = value;
                OnPropertyChanged("Balance");
            }
        }

        private string _Address;

        public string Address
        {
            get
            {
                return _Address;
            }

            set
            {
                this._Address = value;
                OnPropertyChanged("Address");
            }
        }

        private String _TariffName;

        public string TariffName
        {
            get
            {
                return _TariffName;
            }

            set
            {
                this._TariffName = value;
                OnPropertyChanged("TariffName");
            }
        }


        private Status _Status;
        public Status Status
        {
            get
            {
                return _Status;
            }

            set
            {
                this._Status = value;
                OnPropertyChanged("Status");
            }
        }
   
    }
}
