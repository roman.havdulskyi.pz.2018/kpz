﻿using Lab5KPZ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.ViewModel
{
    class ContractViewModel : ViewModelBase
    {
        private Services _Services;

        public Services Services
        {
            get
            {
                return _Services;
            }

            set
            {
                this._Services = value;
                OnPropertyChanged("Services");
            }
        }

        private String _ContractId;

        public string ContractId
        {
            get
            {
                return _ContractId;
            }

            set
            {
                this._ContractId = value;
                OnPropertyChanged("ContractId");
            }
        }

        private DateTime _ContractSigned;

        public DateTime ContractSigned
        {
            get
            {
                return _ContractSigned;
            }

            set
            {
                this._ContractSigned = value;
                OnPropertyChanged("ContractSigned");
            }
        }

        private DateTime _ContractTerm;
        public DateTime ContractTerm
        {
            get
            {
                return _ContractTerm;
            }

            set
            {
                this._ContractTerm = value;
                OnPropertyChanged("ContractTerm");
            }
        }

        private bool _IsContractValid;
        public bool IsContractValid
        {
            get
            {
                return _IsContractValid;
            }

            set
            {
                this._IsContractValid = value;
                OnPropertyChanged("IsContractValid");
            }
        }
    }
}
