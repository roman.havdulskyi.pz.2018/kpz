﻿using Lab5KPZ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.ViewModel
{
    class TariffViewModel : ViewModelBase
    {

        public TariffViewModel()
        {

        }

        private String _Name;

        public String Name
        {
            get
            {
                return _Name;
            }

            set
            {
                this._Name = value;
                OnPropertyChanged("Name");
            }
        }

        private int _Speed;

        public int Speed
        {
            get
            {
                return _Speed;
            }

            set
            {
                this._Speed = value;
                OnPropertyChanged("Speed");
            }
        }

        private double _Price;

        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                this._Price = value;
                OnPropertyChanged("Price");
            }
        }

    }
}
