﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.ViewModel
{
    class AddressViewModel : ViewModelBase
    {

        private String _City;

        public string City
        {
            get 
            {
                return _City;
            }

            set
            {
                this._City = value;
                OnPropertyChanged("City");
            }
        }

        private String _Street;

        public string Street
        {
            get
            {
                return _Street;
            }

            set
            {
                this._Street = value;
                OnPropertyChanged("Street");
            }
        }

        private String _BuildNumber;

        public string BuildNumber
        {
            get
            {
                return _BuildNumber;
            }

            set
            {
                this._BuildNumber = value;
                OnPropertyChanged("BuildNumber");
            }
        }

        private String _FlatNumber;

        public string FlatNumber
        {
            get
            {
                return _FlatNumber;
            }

            set
            {
                this._FlatNumber = value;
                OnPropertyChanged("FlatNumber");
            }
        }
    }
}
