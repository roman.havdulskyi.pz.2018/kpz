﻿using Lab5KPZ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.ViewModel
{
    class ServicesViewModel : ViewModelBase
    {
        private Tariff _Tariff;

        public Tariff Tariff
        {
            get
            {
                return _Tariff;
            }

            set
            {
                this._Tariff = value;
                OnPropertyChanged("Tariff");
            }
        }
    }
}
