﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    [DataContract]
    public class Address
    {
        public String City { set; get; }
        public String Street { set; get; }
        public String BuildNumber { set; get; }
        public String FlatNumber { set; get; }
    }
}
