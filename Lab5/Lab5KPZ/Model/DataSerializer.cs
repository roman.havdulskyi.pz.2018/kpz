﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    class DataSerializer
    {

        public static void SerializeData(string fileName, DataModel data)
        {
            var formatter = new DataContractSerializer(typeof(DataModel));
            var s = new FileStream(fileName, FileMode.Create);
            formatter.WriteObject(s, data);
            s.Close();
        }

        public static DataModel DeserializeData(string fileName)
        {
            var formatter = new DataContractSerializer(typeof(DataModel));
            try
            {
                var s = new FileStream(fileName, FileMode.Open);
                return (DataModel)formatter.ReadObject(s);
            }   catch(Exception e)
            {
                return new DataModel()
                {
                    
                };
            }
        }
    }
}
