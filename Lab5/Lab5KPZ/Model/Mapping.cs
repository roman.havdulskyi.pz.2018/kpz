﻿using AutoMapper;
using Lab5KPZ.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    public class Mapping
    {
        public static IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DataModel, DataViewModel>();
                cfg.CreateMap<DataViewModel, DataModel>();

                cfg.CreateMap<Address, AddressViewModel>();
                cfg.CreateMap<AddressViewModel, Address>();

                cfg.CreateMap<Tariff, TariffViewModel>();
                cfg.CreateMap<TariffViewModel, Tariff>();

                cfg.CreateMap<Services, ServicesViewModel>();
                cfg.CreateMap<ServicesViewModel, Services>();

                cfg.CreateMap<Contract, ContractViewModel>();
                cfg.CreateMap<ContractViewModel, Contract>();

                cfg.CreateMap<Manager, ManagerViewModel>();
                cfg.CreateMap<ManagerViewModel, Manager>();

                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<UserViewModel, User>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }          
    }
}
