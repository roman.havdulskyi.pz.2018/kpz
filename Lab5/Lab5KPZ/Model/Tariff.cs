﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    [DataContract]
    public class Tariff
    {
        [DataMember]
        public String Name { set; get; }

        [DataMember]
        public int Speed { set; get; }

        [DataMember]
        public double Price { set; get; }

    }
}
