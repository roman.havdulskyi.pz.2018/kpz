﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    [DataContract]
    public class Manager
    {
        [DataMember]
        public String Name { set; get; }

        [DataMember]
        public String Surname { set; get; }

        [DataMember]
        public String Address { set; get; }

        [DataMember]
        public Status Status { set; get; }


    }
}
