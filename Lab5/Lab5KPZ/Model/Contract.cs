﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    [DataContract]
    public class Contract
    {
        public Services Services { set; get; }
        public String ContractId { set; get; }
        public DateTime ContractSigned { set; get; }
        public DateTime ContractTerm { set; get; }
        public bool IsContractValid { set; get; }
    }
}
