﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZ.Model
{
    [DataContract]
    public class DataModel
    {

        [DataMember]
        public IEnumerable<User> Users { get; set; }

        [DataMember]
        public IEnumerable<Tariff> Tariffs { get; set; }

        [DataMember]
        public IEnumerable<Manager> Managers { get; set; }

        public static DataModel Load()
        {
            return DataSerializer.DeserializeData(@"C:\Users\Roman\Documents\datamodel.dat");
        }

        public static void Save(DataModel dataModel)
        { 
            DataSerializer.SerializeData(@"C:\Users\Roman\Documents\datamodel.dat", dataModel);
        }
    }
}
