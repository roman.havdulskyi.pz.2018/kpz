﻿using AutoMapper;
using Lab5KPZ.Model;
using Lab5KPZ.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Lab5KPZ
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataModel dataModel;
        private DataViewModel dataViewModel;
        private IMapper _mapper;
        
        public App()
        {
            dataModel = DataModel.Load();
            _mapper = Mapping.Create();
            dataViewModel = _mapper.Map<DataModel, DataViewModel>(dataModel);
            var window = new MainWindow()
            {
                DataContext = dataViewModel
            };
            window.Show();
        }


        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                dataModel = _mapper.Map<DataViewModel, DataModel>(dataViewModel);
                DataModel.Save(dataModel);
            }
            catch (Exception)
            {
                base.OnExit(e);
                throw;
            }
        }
    }

 
}
