﻿using Lab5KPZ.Model;
using Lab5KPZ.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Lab5KPZ.Converters
{
    public class UserRadioButtonCheckedConverter : IValueConverter
    {
        private UserViewModel UserViewModel;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UserViewModel = value as UserViewModel;
            return value != null &&  ((UserViewModel)value).Status.ToString() == (string)parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)parameter == Status.None.ToString())
                UserViewModel.Status = Status.None;
            else if ((string)parameter == Status.Signed.ToString())
                UserViewModel.Status = Status.Signed;
            else
                UserViewModel.Status = Status.Terminated;

            return UserViewModel;
        }
    }
}
