﻿using Lab5KPZ.Model;
using Lab5KPZ.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Lab5KPZ.Converters
{
    public class ManagerRadioButtonCheckedConverter : IValueConverter
    {
        private ManagerViewModel ManagerViewModel;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ManagerViewModel = value as ManagerViewModel;
            return value != null &&  ((ManagerViewModel)value).Status.ToString() == (string)parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)parameter == Status.None.ToString())
                ManagerViewModel.Status = Status.None;
            else if ((string)parameter == Status.Signed.ToString())
                ManagerViewModel.Status = Status.Signed;
            else
                ManagerViewModel.Status = Status.Terminated;

            return ManagerViewModel;
        }
    }
}
