﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace LabKPZDB
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private SqlConnection connection = null;
        private DataViewModel tariffViewModel = null;



        public App()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Console.WriteLine(connectionString);
            connection = new SqlConnection(connectionString);

            try
            {
                // Открываем подключение
                connection.Open();
                Console.WriteLine("Подключение открыто");

                tariffViewModel = new DataViewModel(connection);

                var window = new MainWindow()
                {
                     DataContext = tariffViewModel
                };
                window.Show();

                // Отображаем данные
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Подключение закрыто...");
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            tariffViewModel.SaveModel();
            connection.Close();
            Console.WriteLine("OnExit");
        }

    }

    
}
