﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LabKPZDB
{
    class DataViewModel : ViewModelBase
    {

        SqlCommandBuilder SqlCommandBuilder = null;
        public DataViewModel(SqlConnection connection)
        {
            SetControlVisible = new Command(ChangeControlVisible);
            RemoveSelected = new Command(RemoveSelectedItem);

            LoadData(connection);
        }

        private void LoadData(SqlConnection connection)
        {
            Console.WriteLine($"Fetching data has been started {DateTime.Now}");
            string tariffSqlExpression = "SELECT * FROM Tariffs";
            string billingsSqlExpression = "SELECT * FROM Billings";
            string contractsSqlExpression = "SELECT * FROM Contracts";
            string managersSqlExpression = "SELECT * FROM Managers";
            string refillsSqlExpression = "SELECT * FROM Refills";
            string staticIpsSqlExpression = "SELECT * FROM StaticIpService";
            string usersSqlExpression = "SELECT * FROM Users";

            TariffsAdapter = new SqlDataAdapter(tariffSqlExpression, connection);
            TariffsDataSet = new DataSet();
            TariffsAdapter.Fill(TariffsDataSet);
            TariffsDataView = TariffsDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(TariffsAdapter);

            BillingsAdapter = new SqlDataAdapter(billingsSqlExpression, connection);
            BillingsDataSet = new DataSet();
            BillingsAdapter.Fill(BillingsDataSet);
            BillingsDataView = BillingsDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(BillingsAdapter);

            ContractsAdapter = new SqlDataAdapter(contractsSqlExpression, connection);
            ContractsDataSet = new DataSet();
            ContractsAdapter.Fill(ContractsDataSet);
            ContractsDataView = ContractsDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(ContractsAdapter);

            ManagersAdapter = new SqlDataAdapter(managersSqlExpression, connection);
            ManagersDataSet = new DataSet();
            ManagersAdapter.Fill(ManagersDataSet);
            ManagersDataView = ManagersDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(ManagersAdapter);

            RefillsAdapter = new SqlDataAdapter(refillsSqlExpression, connection);
            RefillsDataSet = new DataSet();
            RefillsAdapter.Fill(RefillsDataSet);
            RefillsDataView = RefillsDataSet.Tables[0].DefaultView;
            SqlCommandBuilder = new SqlCommandBuilder(RefillsAdapter);

            StaticIpsAdapter = new SqlDataAdapter(staticIpsSqlExpression, connection);
            StaticIpsDataSet = new DataSet();
            StaticIpsAdapter.Fill(StaticIpsDataSet);
            StaticIpsDataView = StaticIpsDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(StaticIpsAdapter);

            UsersAdapter = new SqlDataAdapter(usersSqlExpression, connection);
            UsersDataSet = new DataSet();
            UsersAdapter.Fill(UsersDataSet);
            UsersDataView = UsersDataSet.Tables[0].DefaultView;
            new SqlCommandBuilder(UsersAdapter);

            Console.WriteLine($"Fetching data has been finished {DateTime.Now}");
        }

        public ICommand SetControlVisible { set; get; }

        private string _VisibleControl = View.VisibleControl.Users.ToString();

        public string VisibleControl
        {
            get
            {
                return _VisibleControl;
            }

            set
            {
                _VisibleControl = value;
                RemoveSelected = null;
                OnPropertyChanged("VisibleControl");
            }
        }

        public void ChangeControlVisible(object arg)
        {
            VisibleControl = arg.ToString();
        }

        public ICommand RemoveSelected { set; get; }

        public void RemoveSelectedItem(object arg)
        {
            if (SelectedItem == null)
                return;

            SelectedItem.Row.Delete();
            SelectedItem = null;

            SaveModel();
        }

        private DataRowView _SelectedItem = null;

        public DataRowView SelectedItem
        {
            get
            {
                return _SelectedItem;
            }

            set
            {

                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public DataView TariffsDataView { set; get; }
        public DataSet TariffsDataSet { set; get; }
        public SqlDataAdapter TariffsAdapter { set; get; }
        public DataView BillingsDataView { set; get; }
        public DataSet BillingsDataSet { set; get; }
        public SqlDataAdapter BillingsAdapter { set; get; }
        public DataView ContractsDataView { set; get; }
        public DataSet ContractsDataSet { set; get; }
        public SqlDataAdapter ContractsAdapter { set; get; }
        public DataView ManagersDataView { set; get; }
        public DataSet ManagersDataSet { set; get; }

        public SqlDataAdapter ManagersAdapter { set; get; }
        public DataView RefillsDataView { set; get; }
        public DataSet RefillsDataSet { set; get; }

        public SqlDataAdapter RefillsAdapter { set; get; }
        public DataView StaticIpsDataView { set; get; }
        public DataSet StaticIpsDataSet { set; get; }
        public SqlDataAdapter StaticIpsAdapter { set; get; }
        public DataView UsersDataView { set; get; }
        public DataSet UsersDataSet { set; get; }
        public SqlDataAdapter UsersAdapter { set; get; }

        public void SaveModel()
        {
            Console.WriteLine($"Saving data has been started {DateTime.Now}");

            try
            {
                String msg = SqlCommandBuilder.GetDeleteCommand().CommandText;
                TariffsAdapter.Update(TariffsDataSet);
                BillingsAdapter.Update(BillingsDataSet);
                ContractsAdapter.Update(ContractsDataSet);
                ManagersAdapter.Update(ManagersDataSet);
                RefillsAdapter.Update(RefillsDataSet);
                StaticIpsAdapter.Update(StaticIpsDataSet);
                UsersAdapter.Update(UsersDataSet);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message, "Error while commiting changes");
            }
            Console.WriteLine($"Saving data has been finished {DateTime.Now}");

        }

    }
}
