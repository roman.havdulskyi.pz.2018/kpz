﻿using Lab5KPZDB_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.ViewModel
{
    class TariffViewModel : ViewModelBase
    {

        public TariffViewModel()
        {

        }

        private int _Id;
        public int Id
        {
            get
            {
                return _Id;
            }

            set
            {
                this._Id = value;
                OnPropertyChanged("Id");
            }
        }

        private String _Name;

        public String Name
        {
            get
            {
                return _Name;
            }

            set
            {
                this._Name = value;
                OnPropertyChanged("Name");
            }
        }

        private int _Speed;

        public int Speed
        {
            get
            {
                return _Speed;
            }

            set
            {
                this._Speed = value;
                OnPropertyChanged("Speed");
            }
        }

        private double _Price;

        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                this._Price = value;
                OnPropertyChanged("Price");
            }
        }

        private double _DiscountPrice;

        public double DiscountPrice
        {
            get
            {
                return _DiscountPrice;
            }

            set
            {
                this._DiscountPrice = value;
                OnPropertyChanged("DiscountPrice");
            }
        }
        
        private double _ActivateDiscount;

        public double ActivateDiscount
        {
            get
            {
                return _ActivateDiscount;
            }

            set
            {
                this._ActivateDiscount = value;
                OnPropertyChanged("ActivateDiscount");
            }
        }

    }
}
