﻿using Lab5KPZDB_CODEFIRST.Model;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.ViewModel
{
    class ManagerViewModel : ViewModelBase
    {

        private int _Id;
        public int Id
        {
            get
            {
                return _Id;
            }

            set
            {
                this._Id = value;
                OnPropertyChanged("Id");
            }
        }

        private String _Name;
        public String Name
        {
            get
            {
                return _Name;
            }

            set
            {
                this._Name = value;
                OnPropertyChanged("Name");
            }
        }

        private String _Surname;

        public String Surname
        {
            get
            {
                return _Surname;
            }

            set
            {
                this._Surname = value;
                OnPropertyChanged("Surname");
            }
        }

        private string _Address;

        public string Address
        {
            get
            {
                return _Address;
            }

            set
            {
                this._Address = value;
                OnPropertyChanged("Address");
            }
        }

        private int _Status;
        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                this._Status = value;
                OnPropertyChanged("Status");
            }
        }
    }
}
