﻿using AutoMapper;
using Lab5KPZDB_CODEFIRST.ViewModel;
using Lab5KPZDB_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace LabKPZDB_EF_CODEFIRST
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataContext dataContext;
        private DataModel dataModel;
        private DataViewModel dataViewModel;
        private IMapper _mapper;

        public App()
        {
            dataContext = new DataContext();
            dataModel = DataModel.Load(dataContext);
            _mapper = Mapping.Create();
            dataViewModel = _mapper.Map<DataModel, DataViewModel>(dataModel);
            var window = new MainWindow()
            {
                DataContext = dataViewModel
            };
            window.Show();
        }


        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                dataModel = _mapper.Map<DataViewModel, DataModel>(dataViewModel);
                DataModel.Save(dataModel, dataContext);
            }
            catch (Exception)
            {
                base.OnExit(e);
                throw;
            }
        }
    }
}
