﻿namespace LabKPZDB_EF_CODEFIRST.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ispDBv2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tariffs", "DiscountPrice", c => c.Double(nullable: false));
            AddColumn("dbo.Tariffs", "ActivateDiscount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tariffs", "ActivateDiscount");
            DropColumn("dbo.Tariffs", "DiscountPrice");
        }
    }
}
