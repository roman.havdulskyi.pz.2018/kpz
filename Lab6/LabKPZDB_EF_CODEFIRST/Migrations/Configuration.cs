﻿namespace LabKPZDB_EF_CODEFIRST.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Lab5KPZDB_CODEFIRST.Model.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Lab5KPZDB_CODEFIRST.Model.DataContext";
        }

        protected override void Seed(Lab5KPZDB_CODEFIRST.Model.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
