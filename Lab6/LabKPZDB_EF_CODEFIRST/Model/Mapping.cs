﻿using AutoMapper;
using Lab5KPZDB_CODEFIRST.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    public class Mapping
    {
        public static IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DataModel, DataViewModel>();
                cfg.CreateMap<DataViewModel, DataModel>();

                cfg.CreateMap<Tariff, TariffViewModel>();
                cfg.CreateMap<TariffViewModel, Tariff>();

                cfg.CreateMap<Manager, ManagerViewModel>();
                cfg.CreateMap<ManagerViewModel, Manager>();

                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<UserViewModel, User>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }          
    }
}
