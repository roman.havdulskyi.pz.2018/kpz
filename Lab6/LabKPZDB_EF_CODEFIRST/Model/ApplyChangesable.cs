﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabKPZDB_EF_CODEFIRST.Model
{
    public interface ApplyChangesable<E>
    {

        int GetIntID();
        void ApplyChanges(E value);

    }
}
