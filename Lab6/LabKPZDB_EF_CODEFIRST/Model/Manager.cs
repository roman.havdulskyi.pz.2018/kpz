﻿using LabKPZDB_EF_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    public class Manager : ApplyChangesable<Manager>
    {
        public int Id { get; set; }

        public String Name { set; get; }

        public String Surname { set; get; }

        public String Address { set; get; }

        public int Status { set; get; }

        public int GetIntID()
        {
            return Id;
        }

        void ApplyChangesable<Manager>.ApplyChanges(Manager value)
        {
            this.Name = value.Name;
            this.Surname = value.Surname;
            this.Address = value.Address;
            this.Status = value.Status;
        }
    }
}
