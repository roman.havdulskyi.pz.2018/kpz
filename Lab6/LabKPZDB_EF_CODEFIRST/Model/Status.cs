﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    [DataContract]
    public enum Status
    {
        [EnumMember]
        None,

        [EnumMember]
        Signed,

        [EnumMember]
        Terminated    
    }
}
