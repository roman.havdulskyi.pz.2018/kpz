﻿using LabKPZDB_EF_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    public class Tariff : ApplyChangesable<Tariff>
    {
        public int Id { get; set; }

        public String Name { set; get; }

        public int Speed { set; get; }

        public double Price { set; get; }

        public double DiscountPrice { set; get; }

        public int ActivateDiscount { set; get; }


        public int GetIntID()
        {
            return Id;
        }

        void ApplyChangesable<Tariff>.ApplyChanges(Tariff value)
        {
            this.Name = value.Name;
            this.Speed = value.Speed;
            this.Price = value.Price;
            this.DiscountPrice = value.DiscountPrice;
            this.ActivateDiscount = value.ActivateDiscount;
        }
    }
}
