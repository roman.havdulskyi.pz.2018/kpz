﻿using LabKPZDB_EF_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    public class DataModel
    {

        public List<User> Users { get; set; }

        public List<Tariff> Tariffs { get; set; }

        public List<Manager> Managers { get; set; }

        public static DataModel Load(DataContext dataContext)
        {
            return new DataModel()
            {
                Managers = dataContext.Managers.ToList(),
                Tariffs = dataContext.Tariffs.ToList(),
                Users = dataContext.Users.ToList()
            };

        }

        public static void Save(DataModel datamodel, DataContext dataContext)
        {
            var tariffs = dataContext.Tariffs;
            var managers = dataContext.Managers;
            var users = dataContext.Users;

            DBDiffer<Tariff>.ApplyChanges(tariffs, datamodel.Tariffs);
            DBDiffer<User>.ApplyChanges(users, datamodel.Users);
            DBDiffer<Manager>.ApplyChanges(managers, datamodel.Managers);

            dataContext.SaveChanges();
        }


    }
}
