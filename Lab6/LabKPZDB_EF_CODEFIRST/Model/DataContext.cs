﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    
    public class DataContext : DbContext
    {

        public DataContext()
            : base("ISPDatabaseLight2Entities")
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Tariff> Tariffs { get; set; }

        public DbSet<Manager> Managers { get; set; }

       
    }
}
