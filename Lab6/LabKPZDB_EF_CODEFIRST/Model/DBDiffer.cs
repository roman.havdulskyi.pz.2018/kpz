﻿using Lab5KPZDB_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabKPZDB_EF_CODEFIRST.Model
{
    public class DBDiffer<E> where E :class, ApplyChangesable<E>
    {
       public static void ApplyChanges(DbSet<E> dbSet, List<E> newList)
        {

            int pos = 0;

            foreach (var x in dbSet)
            {
                var itemToDelete = newList.FirstOrDefault(delegate (E value)
                {
                    var result = false;
                    if (x is User)
                        result = (x as User).Id == value.GetIntID();
                    else if (x is Manager)
                        result = (x as Manager).Id == value.GetIntID();
                    else if (x is Tariff)
                        result = (x as Tariff).Id == value.GetIntID();
                    
                    return result;
                });
                if (itemToDelete == null)
                    dbSet.Remove(x);
            }

            foreach (var x in newList)
            {

                var itemToChange = dbSet.FirstOrDefault(delegate (E value)
                {
                    var result = false;
                    if (value is User)
                        result = (value as User).Id == x.GetIntID();
                    else if (value is Manager)
                        result = (value as Manager).Id == x.GetIntID();
                    else if (value is Tariff)
                        result = (value as Tariff).Id == x.GetIntID();
          
                    return result;
                });

                if (itemToChange != null)
                {
                    itemToChange.ApplyChanges(x);
                }
                else
                {
                    dbSet.Add(x);
                }
                pos++;
            }
        }

        
    }
}
