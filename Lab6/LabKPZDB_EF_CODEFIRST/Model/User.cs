﻿using LabKPZDB_EF_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab5KPZDB_CODEFIRST.Model
{
    public class User : ApplyChangesable<User>
    {
        public int Id { get; set; }

        public String Name { set; get; }
        public String Surname { set; get; }
        public double Balance { set; get; }
        public string Address { set; get; }
        public string TariffName { set; get; }
        public int Status { set; get; }

        public int GetIntID()
        {
            return Id;
        }

        void ApplyChangesable<User>.ApplyChanges(User value)
        {
            this.Name = value.Name;
            this.Surname = value.Surname;
            this.Balance = value.Balance;
            this.Address = value.Address;
            this.TariffName = value.TariffName;
            this.Status = value.Status;
        }
    }
}
