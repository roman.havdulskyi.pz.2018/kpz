﻿using Lab5KPZDB_CODEFIRST.Model;
using Lab5KPZDB_CODEFIRST.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Lab5KPZDB_CODEFIRST.Converters
{
    public class ManagerRadioButtonCheckedConverter : IValueConverter
    {
        private ManagerViewModel ManagerViewModel;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ManagerViewModel = value as ManagerViewModel;
            return value != null &&  ((ManagerViewModel)value).Status.ToString() == (string)parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            ManagerViewModel.Status = int.Parse(parameter.ToString());
            return ManagerViewModel;
        }
    }
}
