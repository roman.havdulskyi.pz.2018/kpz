﻿using Lab5KPZDB_CODEFIRST.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Lab5KPZDB_CODEFIRST.Converters
{
    class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var signedStatus = (int)value == 0 ? Status.Terminated : Status.Signed;
            var uri = new Uri(string.Format(@"../Resources/StatusIcons/{0}.png", signedStatus), UriKind.Relative);
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
