﻿using Lab5KPZDB_CODEFIRST.Model;
using Lab5KPZDB_CODEFIRST.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Lab5KPZDB_CODEFIRST.Converters
{
    public class UserRadioButtonCheckedConverter : IValueConverter
    {
        private UserViewModel UserViewModel;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UserViewModel = value as UserViewModel;
            return value != null &&  ((UserViewModel)value).Status.ToString() == (string)parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UserViewModel.Status = int.Parse(parameter.ToString());
            return UserViewModel;
        }
    }
}
