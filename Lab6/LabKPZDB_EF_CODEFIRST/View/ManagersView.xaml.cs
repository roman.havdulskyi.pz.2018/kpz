﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab5KPZDB_CODEFIRST.View
{
    /// <summary>
    /// Interaction logic for ManagersView.xaml
    /// </summary>
    public partial class ManagersView : UserControl
    {
        public ManagersView()
        {
            InitializeComponent();
        }

        public string DataGridBackground
        {
            get { return (string)GetValue(DataGridBackgroundProperty); }
            set { SetValue(DataGridBackgroundProperty, value); }
        }

        public static readonly DependencyProperty DataGridBackgroundProperty =
            DependencyProperty.Register("DataGridBackground", typeof(string),
            typeof(ManagersView), new UIPropertyMetadata("Black", DataGridBackgroundChanged));

        private static void DataGridBackgroundChanged(DependencyObject depObj,
         DependencyPropertyChangedEventArgs args)
        {
            ManagersView s = (ManagersView)depObj;
            s.dataGridManagers.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(args.NewValue.ToString()); 
        }
    }
}
