﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace LabKPZDB_EF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataViewModel dataViewModel = null;

        public App()
        {
            dataViewModel = new DataViewModel();

            var mainWindow = new MainWindow()
            {
                DataContext = dataViewModel
            };

            mainWindow.Show();

        }

        protected override void OnExit(ExitEventArgs e)
        {
            Console.WriteLine($"Saving data has been started {DateTime.Now}");
            dataViewModel.SaveData();
            Console.WriteLine($"Saving data has been started {DateTime.Now}");

            Console.WriteLine("OnExit");
        }

    }
}
