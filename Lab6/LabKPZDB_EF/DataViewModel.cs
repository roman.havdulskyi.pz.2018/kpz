﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LabKPZDB_EF
{
    class DataViewModel : ViewModelBase
    {

        private ISPDatabase2Entities1 entities;

        public DataViewModel()
        {
            Console.WriteLine($"Fetching data has been started {DateTime.Now}");

            this.entities = new ISPDatabase2Entities1();

            LoadData();

            Console.WriteLine($"Fetching data has been started {DateTime.Now}");

            SetControlVisible = new Command(ChangeControlVisible);
            RefreshDataCommand = new Command(RefreshData);
            SaveDataCommand = new Command(SaveData);
            RemoveSelected = new Command(RemoveSelectedItem);

        }

        public ICommand SetControlVisible { set; get; }
        public ICommand RefreshDataCommand { set; get; }

        public void RefreshData(object arg)
        {
            LoadData();
        }

        public ICommand SaveDataCommand { set; get; }

        public void SaveData(object arg)
        {
            SaveData();
        }

        private string _VisibleControl = View.VisibleControl.Users.ToString();

        public string VisibleControl
        {
            get
            {
                return _VisibleControl;
            }

            set
            {
                _VisibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public void ChangeControlVisible(object arg)
        {
            VisibleControl = arg.ToString();
            SelectedItem = null;
        }

        public ICommand RemoveSelected { set; get; }

        public void RemoveSelectedItem(object arg)
        {
            if (SelectedItem == null)
                return;

            if (VisibleControl == View.VisibleControl.Managers.ToString())
            {
                ManagersList.Remove(SelectedItem as Manager);
            }
            else if (VisibleControl == View.VisibleControl.Users.ToString())
            {
                UsersList.Remove(SelectedItem as User);
            }
            else if (VisibleControl == View.VisibleControl.Tariffs.ToString())
            {
                TariffsList.Remove(SelectedItem as Tariff);
            }
            else if (VisibleControl == View.VisibleControl.Billings.ToString())
            {
                BillingsList.Remove(SelectedItem as Billing);
            }
            else if (VisibleControl == View.VisibleControl.Contracts.ToString())
            {
                ContractsList.Remove(SelectedItem as Contract);
            }
            else if (VisibleControl == View.VisibleControl.Refills.ToString())
            {
                RefillsList.Remove(SelectedItem as Refill);
            }
            else if (VisibleControl == View.VisibleControl.StaticIps.ToString())
            {
                StaticIpServicesList.Remove(SelectedItem as StaticIpService);
            }
        }

        private object _SelectedItem = null;

        public object SelectedItem
        {
            get
            {
                return _SelectedItem;
            }

            set
            {
               
                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public ObservableCollection<Tariff> TariffsList
        {
            get
            {
                return _TariffsList;
            }

            set
            {
                _TariffsList = value;
                OnPropertyChanged("TariffsList");
            }
        }

        public ObservableCollection<Tariff> _TariffsList { set; get; }

        public ObservableCollection<User> UsersList
        {
            get
            {
                return _UsersList;
            }

            set
            {
                _UsersList = value;
                OnPropertyChanged("UsersList");
            }
        }

        public ObservableCollection<User> _UsersList { set; get; }

        public ObservableCollection<Manager> ManagersList
        {
            get
            {
                return _ManagersList;
            }

            set
            {
                _ManagersList = value;
                OnPropertyChanged("ManagersList");
            }
        }

        public ObservableCollection<Manager> _ManagersList { set; get; }

        public ObservableCollection<Contract> ContractsList
        {
            get
            {
                return _ContractsList;
            }

            set
            {
                _ContractsList = value;
                OnPropertyChanged("ContractsList");
            }
        }

        public ObservableCollection<Contract> _ContractsList { set; get; }

        public ObservableCollection<StaticIpService> StaticIpServicesList
        {
            get
            {
                return _StaticIpServicesList;
            }

            set
            {
               _StaticIpServicesList = value;
                OnPropertyChanged("StaticIpServicesList");
            }
        }

        public ObservableCollection<StaticIpService> _StaticIpServicesList { set; get; }

        public ObservableCollection<Refill> RefillsList
        {
            get
            {
                return _RefillsList;
            }

            set
            {
                _RefillsList = value;
                OnPropertyChanged("RefillsList");
            }
        }

        public ObservableCollection<Refill> _RefillsList { set; get; }

        public ObservableCollection<Billing> BillingsList
        {
            get
            {
                return _BillingsList;
            }

            set
            {
                _BillingsList = value;
                OnPropertyChanged("BillingsList");
            }
        }

        public ObservableCollection<Billing> _BillingsList { set; get; }

        private void LoadData()
        {
            entities = new ISPDatabase2Entities1();
            entities.Tariffs.Load();
            TariffsList = entities.Tariffs.Local;

            entities.Users.Load();
            UsersList = entities.Users.Local;

            entities.Managers.Load();
            ManagersList = entities.Managers.Local;

            entities.Contracts.Load();
            ContractsList = entities.Contracts.Local;

            entities.StaticIpServices.Load();
            StaticIpServicesList = entities.StaticIpServices.Local;

            entities.Refills.Load();
            RefillsList = entities.Refills.Local;

            entities.Billings.Load();
            BillingsList = entities.Billings.Local;
        }

        public void SaveData()
        {
            entities.SaveChanges();
        }


    }
}
