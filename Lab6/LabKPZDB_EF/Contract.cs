//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LabKPZDB_EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contract
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Contract()
        {
            this.Users = new HashSet<User>();
        }
    
        public int cid { get; set; }
        public int tariffId { get; set; }
        public Nullable<int> sid { get; set; }
        public Nullable<int> brssid { get; set; }
        public Nullable<int> brsrid { get; set; }
        public Nullable<System.DateTime> contractSigned { get; set; }
        public Nullable<System.DateTime> contractTerm { get; set; }
        public bool contractIsValid { get; set; }
    
        public virtual BrandedRouter BrandedRouter { get; set; }
        public virtual StaticIpService StaticIpService { get; set; }
        public virtual Tariff Tariff { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
}
