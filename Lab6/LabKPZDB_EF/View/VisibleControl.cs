﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabKPZDB_EF.View
{
    public enum VisibleControl
    {
        Users, Managers, StaticIps, Billings, Tariffs, Refills, Contracts
    }
}
