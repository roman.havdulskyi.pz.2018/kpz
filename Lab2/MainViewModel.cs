﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using KPZ_Lab_2.Annotations;
using Timer = System.Timers.Timer;

namespace KPZ_Lab_2
{
    internal class MainViewModel : INotifyPropertyChanged
    {
        private readonly WorldLogic _worldLogic;
        private readonly Person _person;
        private readonly TaxiService _taxiService;
        private readonly SmartHouseService _smartHouseService;

        public MainViewModel()
        {
            _worldLogic = new WorldLogic(this);
            _person = new Person(this);
            _person.Subscribe(_worldLogic);
            _taxiService = new TaxiService(this);
            _taxiService.Subscribe(_worldLogic);
            _smartHouseService = new SmartHouseService(this);
            _smartHouseService.Subscribe(_worldLogic);
        }
       

        public string LightingImgPath
        {
            get => _smartHouseService.LightingImgPath;
            set
            {
                _smartHouseService.LightingImgPath = value;
                OnPropertyChanged(nameof(LightingImgPath));
            }
        }

        public string HeatingImpPath
        {
            get => _smartHouseService.HeatingImgPath;
            set
            {
                _smartHouseService.HeatingImgPath = value;
                OnPropertyChanged(nameof(HeatingImpPath));
            }
        }

        public string TaxiCost
        {
            get => _taxiService.TaxiCost;
            set
            {
                _taxiService.TaxiCost = value;
                OnPropertyChanged(nameof(TaxiCost));
            }
        }

        public string MsgText
        {
            get => _person.MsgText;
            set
            {
                _person.MsgText = value;
                OnPropertyChanged(nameof(MsgText));
            }
        }


        public string PersonImgPath
        {
            get => _person.PersonImgPath;
            set
            {
                _person.PersonImgPath = value;
                OnPropertyChanged(nameof(PersonImgPath));
            }
        }

        public string TimeText
        {
            get => _worldLogic.TimeText;
            set
            {
                _worldLogic.TimeText = value;
                OnPropertyChanged(nameof(TimeText));
            }
        }

        public string WeatherImgPath
        {
            get => _worldLogic.WeatherImgPath;
            set
            {
                _worldLogic.WeatherImgPath = value;
                OnPropertyChanged(nameof(WeatherImgPath));
            }
        }

        public delegate void DayActivityChanged(DayActivity dayActivity);

        public delegate void WeatherChanged(WeatherType weatherType);

        public enum DayActivity
        {
            Sleep,
            TakingAShower,
            Breakfast,
            GoToWork,
            Working,
            Dinner,
            Working1,
            GoToHome,
            Supper,
            Playing
        }

        public enum WeatherType
        {
            Sunny,
            Rainy,
            Cloudy,
            Foggy
        }

        public class WorldLogic
        {
            public event DayActivityChanged DayTimeChanged;
            public event WeatherChanged WeatherChanged;
            private System.Timers.Timer _aTimer;
            private DayActivity _dayActivity;
            private WeatherType? _weatherType = null;
        

            public string TimeText = "22.00 - 07.00";
            public string WeatherImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\sunny.png";
            private readonly MainViewModel _mainViewModel;

            

            public WorldLogic(MainViewModel mainViewModel)
            {
                this._mainViewModel = mainViewModel;
                _dayActivity = DayActivity.Sleep;
                SetTimer();
            }


            private void SetTimer()
            {
                _aTimer = new Timer(3000);
                _aTimer.Elapsed += OnChangeWeather;
                _aTimer.AutoReset = true;
                _aTimer.Enabled = true;
                _aTimer = new Timer(4500);
                _aTimer.Elapsed += OnChangeDayActivity;
                _aTimer.AutoReset = true;
                _aTimer.Enabled = true;
            }

            private void OnChangeWeather(Object source, ElapsedEventArgs e)
            {
                var rnd = new Random();
                var changeWeather = rnd.Next(1, 100);
                if (changeWeather <= 50 && _weatherType != null)
                    return;

                var weather = rnd.Next(0, 3);
                if (_weatherType != null && ((int)_weatherType) == weather)
                    return;

                switch (weather)
                {
                    case 0:
                        _weatherType = WeatherType.Cloudy;
                        WeatherChanged?.Invoke((WeatherType)_weatherType);
                        _mainViewModel.WeatherImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\clouds-and-sun.png");
                        break;
                    case 1:
                        _weatherType = WeatherType.Foggy;
                        WeatherChanged?.Invoke((WeatherType)_weatherType);
                        _mainViewModel.WeatherImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\calm.png");
                        break;
                    case 2:
                        _weatherType = WeatherType.Rainy;
                        WeatherChanged?.Invoke((WeatherType)_weatherType);
                        _mainViewModel.WeatherImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\060-rain.png");
                        break;
                    case 3:
                        _weatherType = WeatherType.Sunny;
                        WeatherChanged?.Invoke((WeatherType)_weatherType);
                        _mainViewModel.WeatherImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\sunny.png");
                        break;
                }
            }

            private void OnChangeDayActivity(Object source, ElapsedEventArgs e)
            {
                var rnd = new Random();
                var maxDayActivity = (int)DayActivity.Playing;

                var currentActivity = (int)_dayActivity;
                currentActivity++;
                if (currentActivity > maxDayActivity)
                    _dayActivity = DayActivity.Sleep;
                else
                    _dayActivity = (DayActivity)currentActivity;

                ShowTime(_dayActivity);
                DayTimeChanged?.Invoke(_dayActivity);
            }

            private void ShowTime(DayActivity dayActivity)
            {
                switch (dayActivity)
                    {
                        case DayActivity.Working1:
                            _mainViewModel.TimeText = "14.00-17.00";
                            break;
                        case DayActivity.GoToWork:
                            _mainViewModel.TimeText = "08.00-09.00";
                            break;
                        case DayActivity.Sleep:
                            _mainViewModel.TimeText = "22.00-07.00";
                            break;
                        case DayActivity.TakingAShower:
                            _mainViewModel.TimeText = "07.00-07.30";
                            break;
                        case DayActivity.Breakfast:
                            _mainViewModel.TimeText = "07.30-08.00";
                            break;
                        case DayActivity.Working:
                            _mainViewModel.TimeText = "09.00-13.00";
                            break;
                        case DayActivity.Dinner:
                            _mainViewModel.TimeText = "13.00-13.30";
                            break;
                        case DayActivity.Supper:
                            _mainViewModel.TimeText = "18.00-18.30";
                            break;
                        case DayActivity.Playing:
                            _mainViewModel.TimeText = "18.30-22.00";
                            break;
                    }
            }

        }

        public class Person 
        {
            private WeatherType _weatherType;

            public string MsgText = "Night is a time for sleep...";
            public string PersonImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\033-kitty-17.png";

            private MainViewModel mainViewModel;


            public Person(MainViewModel mainViewModel)
            {
                this.mainViewModel = mainViewModel;
            }

            public void Subscribe(WorldLogic worldLogic)
            {
                worldLogic.DayTimeChanged += OnDayActivityChanged;
                worldLogic.WeatherChanged += OnWeatherChanged;
            }

            private void OnDayActivityChanged(DayActivity dayActivity)
            {
                Console.WriteLine($@"Day time {dayActivity}");
                switch (dayActivity)
                {
                    case DayActivity.Sleep:
                        mainViewModel.MsgText = ("Night is a time for sleep...");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\033-kitty-17.png");
                        break;
                    case DayActivity.TakingAShower:
                        mainViewModel.MsgText = ("What a beautiful day?");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\014-kitty-35.png");
                        break;
                    case DayActivity.Breakfast:
                        mainViewModel.MsgText = ("Start Your beautiful day with a cup of coffee");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\045-kitty-5.png");
                        break;
                    case DayActivity.GoToWork:
                        mainViewModel.MsgText = (_weatherType != WeatherType.Rainy
                            ? "Let`s go to work and save the world!"
                            : "Wow, I forget an my umbrella...");
                        mainViewModel.PersonImgPath = (_weatherType != WeatherType.Rainy
                            ? @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\036-kitty-14.png"
                            : @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\020-kitty-29.png");
                        break;
                    case DayActivity.Working:
                    case DayActivity.Working1:
                        mainViewModel.MsgText = ("What do we have for work today?");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\016-kitty-33.png");
                        break;
                    case DayActivity.Dinner:
                        mainViewModel.MsgText = ("Take a break and taste a jam");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\007-kitty-42.png");
                        break;
                    case DayActivity.Supper:
                        mainViewModel.MsgText = ("Let`s cook smth tasty");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\034-kitty-16.png");
                        break;
                    case DayActivity.Playing:
                        mainViewModel.MsgText = ("Let`s have a fun");
                        mainViewModel.PersonImgPath = (@"C:\Users\Roman\source\repos\KPZ_Lab_2\images\003-kitty-45.png");
                        break;
                }
            }

            private void OnWeatherChanged(WeatherType weatherType)
            {
                this._weatherType = weatherType;
            }

        }

        public class TaxiService
        {
            private DayActivity _dayActivity;
            private WeatherType _weatherType;
            public string TaxiCost = "Low";
            private readonly MainViewModel _mainViewModel;


            public TaxiService(MainViewModel mainViewModel)
            {
                this._mainViewModel = mainViewModel;
            }

            public void Subscribe(WorldLogic worldLogic)
            {
                worldLogic.DayTimeChanged += OnDateTimeChanged;
                worldLogic.WeatherChanged += OnWeatherChanged;
            }

            private void OnDateTimeChanged(DayActivity dayActivity)
            {
                this._dayActivity = dayActivity;
                _mainViewModel.TaxiCost = CalculateAvgPrice();
            }

            private void OnWeatherChanged(WeatherType weatherType)
            {
                this._weatherType = weatherType;
                _mainViewModel.TaxiCost = CalculateAvgPrice();
            }

            private string CalculateAvgPrice()
            {
                string avgPrice;
                if (_dayActivity == DayActivity.GoToHome || _dayActivity == DayActivity.GoToWork)
                    avgPrice = _weatherType == WeatherType.Rainy ? "The highest" : "Slightly higher";
                else if (_dayActivity != DayActivity.Sleep)
                    avgPrice = _weatherType == WeatherType.Rainy ? "Slightly higher" : "Normal";
                else
                    avgPrice = _weatherType == WeatherType.Rainy ? "Normal" : "Low";

                return avgPrice;
            }

        }

        public class SmartHouseService
        {

            public string LightingImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\lighting_off.png";
            public string HeatingImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\heating_off.png";
            private readonly MainViewModel _mainViewModel;

            public SmartHouseService(MainViewModel mainViewModel)
            {
                this._mainViewModel = mainViewModel;
            }

            public void Subscribe(WorldLogic worldLogic)
            {
                worldLogic.WeatherChanged += (WeatherType a) =>
                {
                    OnWeatherTypeChanged(a);
                };

                worldLogic.DayTimeChanged += OnDayActivityChanged;
            }

            private void OnWeatherTypeChanged(WeatherType weatherType)
            {
                switch (weatherType)
                {
                    case WeatherType.Sunny:
                    case WeatherType.Cloudy:
                    case WeatherType.Foggy:
                        SwitchOffHeating();
                        break;
                    case WeatherType.Rainy:
                        switchOnHeating();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(weatherType), weatherType, null);
                }
            }

            private void OnDayActivityChanged(DayActivity dayActivity)
            {
                switch (dayActivity)
                {
                    case DayActivity.Sleep:
                        SwitchOffLighting();
                        break;
                    case DayActivity.TakingAShower:
                        switchOnLighting();
                        break;
                    case DayActivity.GoToWork:
                        SwitchOffLighting();
                        break;
                    case DayActivity.Supper:
                        switchOnLighting();
                        break;
                }
            }

            private void SwitchOffLighting()
            {
                _mainViewModel.LightingImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\lighting_off.png";
            }

            private void switchOnLighting()
            {
                _mainViewModel.LightingImgPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\lighting_on.png";
            }
            private void switchOnHeating()
            {
                _mainViewModel.HeatingImpPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\heating_on.png";
            }
            private void SwitchOffHeating()
            {
                _mainViewModel.HeatingImpPath = @"C:\Users\Roman\source\repos\KPZ_Lab_2\images\heating_off.png";
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
