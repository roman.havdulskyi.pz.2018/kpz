﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace HavdulskyiAlgorithm2
{
    class RobotUtils
    {
        
        public static Robot.Common.Robot FindRobot(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position == cell)
                    return robot;
            }
            return null;
        }
        public static bool IAmOnStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            foreach (var energyStation in map.Stations)
                if (DistanceHelper.FindDistance(energyStation.Position, movingRobot.Position) <= 1)
                    return true;
            return false;
        }
        public static EnergyStation FindStationNearRobot(Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, Map map)
        {
            EnergyStation nearest = null;
            foreach (var station in map.Stations)
            {
                Position position = station.Position;
                if (IAmOnStation(robot, map, robots))
                    nearest = station;
            }
            return nearest;
        }
        public static bool IsRobotStationEmpty(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map)
        {
            var station = RobotUtils.FindStationNearRobot(movingRobot, robots, map);
            return station != null && station.Energy <= 10;
        }

        public static int CalculateRobotsNearThis(EnergyStation energy, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot)
        {
            return robots.Count(it => movingRobot != null && (it.Position != movingRobot.Position && DistanceHelper.FindDistance(energy.Position, it.Position) <= 10));
        }

        public static int CalculateStationNearThisRobot( Map map, Robot.Common.Robot movingRobot)
        {
            return map.Stations.Count(it =>  DistanceHelper.FindDistance(movingRobot.Position, it.Position) <= 2);
        }

        public static int CalculateRobotsNearThis(Position position, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot, int distance)
        {
            return robots.Count(it => movingRobot != null && (it.Position != movingRobot.Position && DistanceHelper.FindDistance(position, it.Position) <= distance));
        }
        public static int CalculateMyRobotsNearThis(Position position, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot, int distance)
        {
            return robots.Count(it => movingRobot != null && (it.Position != movingRobot.Position && it.OwnerName.Equals(movingRobot.OwnerName) && DistanceHelper.FindDistance(position, it.Position) <= distance));
        }

        public static int CalculateStationNearThis(Position position, Map map, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot)
        {
            return map.Stations.Count(it => (movingRobot == null || it.Position != movingRobot.Position) && DistanceHelper.FindDistance(position, it.Position) <= 2);
        }

        public static int CalculateEnergyNearThis(Position position, Map map, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot)
        {
            return map.Stations.Where(it => DistanceHelper.FindDistance(position, it.Position) <= 2).Select(it=> it.Energy).Sum();
        }

        public static int CalculateEnergyNearThis2(Position position, Map map, IList<Robot.Common.Robot> robots, Robot.Common.Robot movingRobot)
        {
            return map.Stations.Where(it => ( DistanceHelper.FindDistance(position, it.Position) <= 2)).Select(it => it.RecoveryRate).Sum();
        }

        public static bool EnemyRobotsAbleToAttack(Position position, Map map, IList<Robot.Common.Robot> robots, string enemyName, Robot.Common.Robot movingRobot)
        {
            return robots.Any(it => it.OwnerName.Equals(enemyName) &&
                                    (movingRobot.Energy / 10 < (DistanceHelper.FindDistance(it.Position, position) + 30)));
        }
    }
}
