﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace HavdulskyiAlgorithm2
{
    public class HavdulskyiAlgorithm2 : IRobotAlgorithm
    {

        public HavdulskyiAlgorithm2()
        {
            Robot.Tournament.Logger.OnLogRound += Logger_OnLogRound;
            Robot.Tournament.Logger.OnLogMessage += Logger_OnLogMessage;
            this._historyDictionary = new Dictionary<int, int>();
            this._aggressorsHistory = new Dictionary<string, int>();
        }

       

        private void Logger_OnLogMessage(object sender, Robot.Tournament.LogEventArgs e)
        {
            if (!e.Message.Contains("Atk")) return;
            var value = _aggressorsHistory.ContainsKey(e.OwnerName) ? _aggressorsHistory[e.OwnerName] : 0;
            if (_aggressorsHistory.ContainsKey(e.OwnerName))
                _aggressorsHistory.Remove(e.OwnerName);
            _aggressorsHistory[e.OwnerName] = ++value;
            AllowAggressiveTactic = true;
        }

        private void Logger_OnLogRound(object sender, Robot.Tournament.LogRoundEventArgs e)
        {
            CurrentRound++;
        }

        private readonly Dictionary<int, int> _historyDictionary;

        private readonly Dictionary<string, int> _aggressorsHistory;

        public int CurrentRound { set; get; }
        public int RobotCount { set; get; } = 0;
        public bool AllowAggressiveTactic { get; set; }


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            RobotCommand robotCommand = null;
            int lastChangedRound = 0;
            if(_historyDictionary.ContainsKey(robotToMoveIndex))
                lastChangedRound  = _historyDictionary[robotToMoveIndex];

            var movingRobot = robots[robotToMoveIndex];
            var scannerDistance = CurrentRound >= 40 ? 1 :  (RobotCount >= 100 ? 5 : 10);


            if ((AllowAggressiveTactic && (!IsAttackFreeRound() || (CurrentRound >= 43 && IsNoOneStationNearRobot(map, movingRobot)))) && movingRobot.Energy >= 150)
            {
                int maxDistance = CurrentRound >= 45 ? 2000 : 1000;
                var nearestRobots = robots.Where(it => movingRobot.Energy >= 800 && DistanceHelper.FindDistance(movingRobot.Position, it.Position) <= maxDistance).ToList();

                if (nearestRobots.Count > 0)
                {

                    nearestRobots.Sort((it, it1) =>
                    {
                        var res = DistanceHelper.FindDistance(movingRobot.Position, it.Position)
                            .CompareTo(DistanceHelper.FindDistance(movingRobot.Position, it1.Position));
                        if (res == 0)
                            res = it1.Energy.CompareTo(it.Energy);
                        return res;
                    });

                    var aggressiveEnemyRobots = nearestRobots.Where(it =>
                        {
                            var res = it.OwnerName != Author && _aggressorsHistory.ContainsKey(it.OwnerName);
                            if (res)
                            {
                                var requiredEnergy = (DistanceHelper.FindDistance(it.Position, movingRobot.Position) + 30);
                                res = it.Energy >= requiredEnergy && (movingRobot.Energy / 10 >= requiredEnergy) && RobotUtils.CalculateMyRobotsNearThis(it.Position, robots, movingRobot, 2) <= 1;
                            }

                            return res;
                        })
                        .ToList();

                    if (aggressiveEnemyRobots.Count > 0 && CurrentRound <= 38)
                    {
                        if (nearestRobots.Count > 0)
                            robotCommand = new MoveCommand() { NewPosition = aggressiveEnemyRobots.First().Position };
                    }
                    else
                    {

                        nearestRobots = nearestRobots.Where(it =>
                            {
                                var res = it.OwnerName != Author;
                                if (res)
                                    res = (it.Energy / 10 >
                                           (DistanceHelper.FindDistance(it.Position, movingRobot.Position) +
                                            30 + (CurrentRound <= 40 ? 200 : 80)));
                                return res;
                            })
                            .ToList();

                        if (nearestRobots.Count > 0)
                            robotCommand = new MoveCommand() {NewPosition = nearestRobots.First().Position};
                    }
                }
            } 

            if (robotCommand == null && (IsChangePositionRound() || CanChangePosition(movingRobot, map, robots, lastChangedRound, scannerDistance)))
            {

                var tempPosition =  FindBestPosition(robots, robotToMoveIndex, map, scannerDistance);

                if (tempPosition != null && tempPosition != movingRobot.Position)
                {
                    robotCommand = new MoveCommand() {NewPosition = tempPosition};
                    if (_historyDictionary.ContainsKey(robotToMoveIndex))
                        _historyDictionary.Remove(robotToMoveIndex);
                    _historyDictionary[robotToMoveIndex] = CurrentRound;
                }
                

            }

            if (robotCommand == null)
            {
                CreateOrCollect(movingRobot, robots, out robotCommand);
            }

            return robotCommand;
        }

        private void CreateOrCollect(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, out RobotCommand command)
        {
            if (movingRobot.Energy >= 300 && RobotCount <= 100)
            {
                RobotCount++;
                command = new CreateNewRobotCommand();
            }
            else 
            {
                command = new CollectEnergyCommand();
            }
           
        }

        private bool IsAttackFreeRound()
        {
            return RobotCount < 100 || (CurrentRound >= 39 && CurrentRound <= 46);
        }

        private bool IsNoOneStationNearRobot(Map map, Robot.Common.Robot movingRobot)
        {
            return RobotUtils.CalculateStationNearThisRobot(map, movingRobot) == 0;
        }

        private bool CanChangePosition(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, int lastChangedRound, int scannerDistance)
        {
            return (CurrentRound <= 38 &&
                   (lastChangedRound == 0 || (RobotCount >= 100 && CurrentRound - lastChangedRound >= 50)) ||
                   HasRobotsNearThis(movingRobot, robots, scannerDistance)) || IsNoOneStationNearRobot(map, movingRobot);
        }

        private bool HasRobotsNearThis(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, int scannerDistance)
        {
           return RobotUtils.CalculateRobotsNearThis(movingRobot.Position, robots, movingRobot, scannerDistance) > 0;
        }

        private bool IsChangePositionRound()
        {
            return CurrentRound == 39;
        }

        private bool CanCollectEnergy(IList<Robot.Common.Robot> robots)
        {
            bool canCollect = false;
            if (!AllowAggressiveTactic || CurrentRound >= 40)
            {
                canCollect = true;
            }
            else
            {
                Dictionary<string, int> dictionary = new Dictionary<string, int>();
                var myEnergy = 0;
                foreach (var robot in robots)
                {

                    if (robot.OwnerName == Author)
                        myEnergy += robot.Energy;
                    else
                    {
                        int energy = 0;
                        if (dictionary.ContainsKey(robot.OwnerName))
                        {
                            energy = dictionary[robot.OwnerName];
                            dictionary.Remove(robot.OwnerName);
                        }

                        energy += robot.Energy;
                        dictionary[robot.OwnerName] = energy;
                    }

                }

                var energyList = dictionary.Values.ToList();
                var maxEnergy = energyList.Max();
                var minEnergy = energyList.Min();
                canCollect = energyList.Count == 0 || myEnergy < maxEnergy * 0.8  || myEnergy < minEnergy;
            }

            return canCollect;
        }

        public string Author { get; } = "Roman Havdulskyi";

        public Position FindBestPosition(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map, int distance)
        {

            var movingRobot = robots[robotToMoveIndex];
            var energyNearRobot = RobotCount >=100 ? RobotUtils.CalculateEnergyNearThis(movingRobot.Position, map, robots, movingRobot) : RobotUtils.CalculateEnergyNearThis2(movingRobot.Position, map, robots, movingRobot);
            int robotsNear = RobotUtils.CalculateRobotsNearThis(movingRobot.Position, robots, movingRobot, distance);
            int maxEnergy = CurrentRound >= 35 ? 50 : (RobotCount >= 100 ? 100 : movingRobot.Energy); 
            var list = map.GetNearbyResources(movingRobot.Position,  Math.Min(movingRobot.Energy, maxEnergy));
            double p1 = 2 * (maxEnergy * maxEnergy);
            double p2 = (maxEnergy * maxEnergy * Math.Cos(45));
            double diff = Math.Sqrt(p1 - p2)/2;
            var firstPos = new Position((int) Math.Max(movingRobot.Position.X - diff, 0), (int) Math.Max(movingRobot.Position.Y - diff, 0));
            var lastPos = new Position((int)Math.Min(movingRobot.Position.X + diff, map.MaxPozition.X), (int)Math.Min(movingRobot.Position.Y + diff, map.MaxPozition.Y));
            var maxPossibleEnergy = robotsNear >0 ? 0 :  energyNearRobot;
            var minDistance  = 0;
            Position result = null;
            for (int i = firstPos.X; i < lastPos.X; i++)
            {
                for (int j = firstPos.Y; j < lastPos.Y; j++)
                {
                    var tempPosition = new Position(x: i, j);
                  
                    if(tempPosition == movingRobot.Position)
                        continue;
                    if (DistanceHelper.FindDistance(movingRobot.Position, tempPosition) <= movingRobot.Energy)
                    {
                        if (CurrentRound <= 40 || CurrentRound >= 45)
                        {
                            foreach (var item in _aggressorsHistory)
                            {
                                if (!item.Key.Equals(Author) && item.Value > 5)
                                {
                                    if (RobotUtils.EnemyRobotsAbleToAttack(tempPosition, map, robots, item.Key,
                                        movingRobot))
                                        continue;
                                }
                            }
                        }

                        if (RobotUtils.CalculateRobotsNearThis(tempPosition, robots, movingRobot,  distance) == 0)
                        {
                            var currDistance = DistanceHelper.FindDistance(tempPosition, movingRobot.Position);
                            var stationEnergyCount = RobotCount >= 100 ?
                                RobotUtils.CalculateEnergyNearThis(tempPosition, map, robots, movingRobot) : RobotUtils.CalculateEnergyNearThis2(tempPosition, map, robots, movingRobot);
                            if (energyNearRobot <= stationEnergyCount && (stationEnergyCount > maxPossibleEnergy || (stationEnergyCount == maxPossibleEnergy && currDistance < minDistance)))
                            {
                                maxPossibleEnergy = stationEnergyCount;
                                result = tempPosition;
                                minDistance = currDistance;
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
